package vue;
/**
 * PannInfoReseau est le panneau qui affiche toutes les informations relatives au r�seau
 * @author Achraf Zineddine Elidrissi | ETS
 */
import modele.gestionnaires.GestionnaireJeu;
import modele.reseau.GestionnaireReseau;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PannInfoReseau extends AbstractPanneauInfo {


    /** Instantiation des objets reseau et jeu **/
    GestionnaireReseau reseau = GestionnaireReseau.getInstance();
    GestionnaireJeu jeu = GestionnaireJeu.getInstance();
    ConstantesVue cvue = new ConstantesVue();


    /** Deffinition des JLabel relatif au panneau **/
    JLabel lblnbConversation;
    JLabel lblcellulaire;
    JLabel lblnbCriminel;

    /** Attribut relatif au panneau **/
    int nbCellulaires, nbCriminels, nbConvo, updateNbConvo;



    @Override
    public void initPanneau(){
        this.setBackground(Color.gray);
        Border Whiteline = BorderFactory.createLineBorder(Color.white);
        this.setBorder(Whiteline);
        this.setLayout(new GridLayout(4,1));


        /** Ajout du titre au panneau en appliquant le font de ConstantesVue **/
        Titre = new JLabel("Informations R�seau", JLabel.CENTER);
        Titre.setFont(cvue.POLICE_TITRES);
        Titre.setForeground(Color.WHITE);


        /** TIMER pour rafraichir le panneau informations reseau **/
        int retard = 250; // millisecondes
        ActionListener taskPerformer = new ActionListener () {
            public void actionPerformed (ActionEvent evt) {

                updateNbConvo = jeu.getNbConversations();
                initContenu();
            }
        };
        new Timer(retard,taskPerformer).start();
    }


    @Override
    public void initContenu(){

        removeAll();
        refresh();
    }

    public void refresh(){

        /** Cr�ation et Ajout du label NBcellulaires au panneau **/
        lblcellulaire = new JLabel("Nb Cellulaires: " + nbCellulaires, JLabel.CENTER);
        lblcellulaire.setFont(cvue.POLICE_ETIQUETTES);
        lblcellulaire.setForeground(Color.WHITE);
        nbCellulaires = reseau.NB_CELLULAIRES;


        /** Cr�ation  et Ajout du label NBcriminels au panneau **/
        lblnbCriminel = new JLabel("Nb Criminels: " + nbCriminels, JLabel.CENTER);
        lblnbCriminel.setForeground(Color.WHITE);
        lblnbCriminel.setFont(cvue.POLICE_ETIQUETTES);
        nbCriminels = reseau.NB_CRIMINELS;


        /** Cr�ation  et Ajout du label NBconversations au panneau **/
        lblnbConversation = new JLabel("Nb conversation en cours: " + nbConvo, JLabel.CENTER);
        lblnbConversation.setForeground(Color.WHITE);
        lblnbConversation.setFont(cvue.POLICE_ETIQUETTES);
        nbConvo = updateNbConvo;


        /** Refresh de la page **/
        updateUI();
        add(Titre);
        add(lblcellulaire);
        add(lblnbCriminel);
        add(lblnbConversation);
    }
}
