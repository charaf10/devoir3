package vue;
/**
 * AbstractPanneauInfo est une classe abstraite, de laquelle d�rive
 * InformationsJeu, OperationIMSI et PanInfoReseau
 * @author Achraf Zineddine Elidrissi | ETS
 */
        import modele.reseau.GestionnaireReseau;
        import observer.MonObserver;
        import javax.swing.*;
        import javax.swing.border.Border;
        import java.awt.*;


        abstract public class AbstractPanneauInfo extends JPanel implements MonObserver {

            /** Cr�ation d'un label Titre et centr� **/
            JLabel Titre = new JLabel("Titre", JLabel.CENTER);


    public AbstractPanneauInfo(){

        initPanneau();
        initContenu();
    }

    public void initPanneau(){

        /** background en gris pour les trois panneau qui derive de cette classe **/
        Titre.setForeground(Color.WHITE);

        /** Border en blanc pour les trois panneau qui derive de cette classe **/
        Border Whiteline = BorderFactory.createLineBorder(Color.white);
        this.setBorder(Whiteline);

        validate();
        repaint();
    }


     public void initContenu(){

         /** ajoute le JLabel titre aux trois panneaux **/
         add(Titre);
     }

    @Override
    public void avertir() {
        validate();
        repaint();}

}

