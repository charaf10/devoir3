package vue;
/**
 * PannInfoReseau est le panneau qui affiche toutes les informations relatives
 * aux op�rations de la session de jeu (Achat IMSI, IMSI poss�d� et IMSI plac�)
 * @author Achraf Zineddine Elidrissi | ETS
 */
import modele.gestionnaires.GestionnaireJeu;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperationIMSI extends AbstractPanneauInfo {


    /** Instantiation d'une session de jeu (GestionnaireJeu) **/
    GestionnaireJeu jeu = GestionnaireJeu.getInstance();
    ConstantesVue cvue = new ConstantesVue();

    /** Attribut relatif � la session de jeu **/
    int nbImsiPlace;
    int nbImsiRestant;
    JLabel imsiRestant;
    JLabel imsiPlacee;


    @Override
    public void initPanneau(){
        this.setBackground(Color.gray);
        Border Whiteline = BorderFactory.createLineBorder(Color.white);
        this.setBorder(Whiteline);
        this.setLayout(new GridLayout(4,1));

        nbImsiRestant = jeu.NB_IMSI_DEPART;

        /** Ajout du titre au panneau en appliquant le font de ConstantesVue **/
        Titre = new JLabel("Op�ration IMSI", JLabel.CENTER);
        Titre.setFont(cvue.POLICE_TITRES);
        Titre.setForeground(Color.WHITE);
        add(Titre);


        /** TIMER pour rafraichir le panneau Op�rations IMSI **/
        int retard = 250; // millisecondes
        ActionListener taskPerformer = new ActionListener () {
            public void actionPerformed (ActionEvent evt) {
                initContenu();
            }
        };
        new Timer(retard,taskPerformer).start();
    }

    public void initContenu(){
        removeAll();
        updatePoint();
    }

    public void updatePoint(){
        GestionnaireJeu jeu = GestionnaireJeu.getInstance();

        /** Cr�ation et ajout des labels imsiRestant et imsiPlacee au panneau OperationIMSI **/
        imsiRestant = new JLabel("Nombre d'IMSI possed�: " + nbImsiRestant, JLabel.CENTER);
        imsiRestant.setForeground(Color.WHITE);
        imsiRestant.setFont(cvue.POLICE_ETIQUETTES);
        nbImsiRestant = jeu.getnbImsiRestant();

        imsiPlacee = new JLabel("Nombre d'IMSI plac�: " + nbImsiPlace, JLabel.CENTER);
        imsiPlacee.setForeground(Color.WHITE);
        imsiPlacee.setFont(cvue.POLICE_ETIQUETTES);
        nbImsiPlace = jeu.getnbImsiplace();


        /** mise � jour  des labels Titre, imsiRestant et imsiPlacee au panneau OperationIMSI **/
        add(Titre);
        add(imsiRestant);
        add(imsiPlacee);

        /** cr�ation du button achat IMSI **/
        JButton ACHAT_IMSI = new JButton("Acheter IMSI");
        add(ACHAT_IMSI);

        /** Ajout d'un ActionListener au button pour achat IMSI **/
        ACHAT_IMSI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                /** Si le nombre de points est plus grand ou �gal au prix d'un IMSI, Effectue l'achat **/
                if (jeu.pointage() >= jeu.COUT_IMSI)
                {
                    jeu.deletePointApresAchat();
                    jeu.add1ImsiRestant();
                    int result = JOptionPane.showConfirmDialog(null,
                            "Felicitation, vous avez achet� un nouveau IMSI",
                            "Achat IMSI: ",
                            JOptionPane.CLOSED_OPTION);

                    /** Refresh du panneau **/
                    updateUI();


                    /** Sinon, si le nombre de points n'est pas suffisant, refuse l'achat
                     * et renvoi un message pour prevenir le user **/
                }else if (jeu.pointage() < jeu.COUT_IMSI)
                {
                    int result = JOptionPane.showConfirmDialog(null,
                            "Vous n'avez pas assez de points pour acheter un IMSI", "Message: ",
                            JOptionPane.CLOSED_OPTION);

                }
            }
        });

        add(ACHAT_IMSI) ;
    }
}
