package vue;
/**
 * PanControle est le panneau qui contient les panneaux PannInfoReseau, InformationJeu et OperationIMSI
 * @author Achraf Zineddine Elidrissi | ETS
 */

import modele.physique.Carte;
import javax.swing.*;
import java.awt.*;


public class PanControle extends JPanel {


    private static int RAYON_ANTENNE = 20;
    private static int RAYON_INDIVIDU = 10;


    public PanControle()
    {
        initPanneau();
        initContenu();
        setVisible(true);
    }

    public void initPanneau() {

        /** deffinition d'une hauteur de 1/8 du panneau dessin **/
        int hauteur = ((int) Carte.DIMENSION_JEU.getX()/8);

        this.setBackground(Color.black);
        this.setPreferredSize(new Dimension(400,hauteur));
        this.setLayout(new GridLayout(1,3));

    }
        public void initContenu(){
        /** Vide **/
        }
    }