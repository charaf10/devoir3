package vue;

/**
 * Panneau affichant le r�seau cellulaire � l'�cran et traitant les clics de souris.
 * 
 * @author Fred Simard | ETS
 * @revision hiver 2021
 */

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import modele.gestionnaires.GestionnaireJeu;
import modele.physique.Carte;
import modele.physique.Position;
import modele.reseau.Antenne;
import modele.reseau.Cellulaire;
import modele.reseau.GestionnaireReseau;
import modele.reseau.IntercepteurOndes;
import observer.MonObserver;

@SuppressWarnings("serial")
public class PanneauDessin extends JPanel implements MonObserver {

	private static int RAYON_ANTENNE = 20;
	private static int RAYON_INDIVIDU = 10;


	Dimension taille = new Dimension();
	GestionnaireReseau reseau = GestionnaireReseau.getInstance();
	GestionnaireJeu jeu = GestionnaireJeu.getInstance();

	int nbrInterPlace = 0;
	int nbrInterpossede = jeu.getnbImsiRestant();

	/**
	 * Constructeur du panneau
	 * @param taille du dessin
	 */
	public PanneauDessin(Dimension taille) {

		// initialise la taille du panneau
		this.taille.width = (int) Carte.DIMENSION_JEU.getX();
		this.taille.height = (int) Carte.DIMENSION_JEU.getY();
		this.setPreferredSize(this.taille);

		initDessin();
		initContenu();

		// s'attache au r�seau
		reseau.attacherObserver(this);
	}

	/**
	 * initialise le dessin
	 */
	private void initDessin(){
		validate();
		repaint();
	}


	public void initContenu() { gererClic(); }


		/**
         * Callback pour dessiner dans le panneau dessin
         */
	public void paintComponent(Graphics g) {

		// convertie en engin graphique 2D
		Graphics2D g2 = (Graphics2D) g;

		// appel au paint de base
		super.paintComponent(g);
		// efface l'écran
		g2.clearRect(0, 0, taille.width, taille.height);

		/** affiche Antennes et cellulaires sur la map **/
		dessineAntennes(g2);
		dessineCellulaires(g2);

        //gets rid of the copy
        g2.dispose();
	}

	/**
	 * M�thode pour dessiner les cellulaires � l'�cran
	 * @param g engin graphique
	 */
	private void dessineCellulaires(Graphics g) {

		ArrayList<Cellulaire> cellulaires = reseau.getCellulaires();

		for(Cellulaire cellulaire : cellulaires) {

			Position position = cellulaire.getPosition();

			if(cellulaire.estCriminel()) {
				if(cellulaire.estConnecte()) {
					g.setColor(Color.RED);
				}else {
					g.setColor(Color.BLUE);
				}

			}else {
				g.setColor(Color.BLUE);
			}

			g.fillOval((int)position.getX()-RAYON_INDIVIDU, (int)position.getY()-RAYON_INDIVIDU, 2*RAYON_INDIVIDU, 2*RAYON_INDIVIDU);

			// affiche les liens cellulaires-antenne (contribution: Mathieu S�vigny-Lavall�e)
			g.drawLine((int)cellulaire.getPosition().getX(),(int)cellulaire.getPosition().getY(), (int)cellulaire.getAntenneConnecte().getPosition().getX(),(int)cellulaire.getAntenneConnecte().getPosition().getY());

		}
	}

	/**
	 * M�thode pour dessiner les antennes � l'�cran
	 * @param g engin graphique
	 */

	private void dessineAntennes(Graphics g) {

			ArrayList<Antenne> antennes = reseau.getAntennes();

			for(Antenne antenne : antennes) {

				Position position = antenne.getPosition();

				if (antenne instanceof IntercepteurOndes)
				{
					g.setColor(Color.ORANGE);

				} else { g.setColor(Color.DARK_GRAY); }
				g.fillOval((int)position.getX()-RAYON_ANTENNE, (int)position.getY()-RAYON_ANTENNE, 2*RAYON_ANTENNE, 2*RAYON_ANTENNE);
			}
		}


	/** Methode pour gerer le mouseListener **/
	private void gererClic() {

		/** Création d'une ArrayList d'antennes et
		 * recuperation des antennes dans ArrayList Antennes du gestionnaire réseau  **/
		ArrayList<Antenne> antennes = reseau.getAntennes();

		addMouseListener ( new MouseAdapter() {
			public  void mousePressed (MouseEvent me) {

				nbrInterpossede = jeu.getnbImsiRestant();

				/** Si le button gauche est cliqué, Ajout d'un IMSI sur la map **/
				if (me.getButton() == 1)
				{
					nbrInterPlace = jeu.getnbImsiplace();

					/** s'il y a des IMSI libre à placer, place l'IMSI sur la map **/
					if (nbrInterpossede > nbrInterPlace)
					{
						nbrInterPlace++;
						jeu.add1ImsiPlace();
						reseau.ajouterInterceptor(me);
					}
					else
					{
						int result = JOptionPane.showConfirmDialog(null,
								"Tous vos IMSI sont sur la map",
								"Confirmation de sortie: ",
								JOptionPane.CLOSED_OPTION);
					}
				}

				/** Si le button droit est cliqué, Retrait d'un IMSI de la map **/
				else if(me.getButton() == 3)
				{
					/** compteur : va servir à recuperer l'index du IMSI qui a été cliqué **/
					int i=0;

					try {
						/** si la liste antenne contient des IMSI  **/
						if (antennes.size() > reseau.NB_ANTENNES)
						{
							/** Parcourir les positions de tous les IMSI **/
							for(Antenne antenne : antennes) {

								Position position = antenne.getPosition();

								/** Recuperation du IMSI qui a été Cliqué **/
								if (me.getX()+RAYON_ANTENNE >= position.getX() && me.getX() < position.getX() + RAYON_ANTENNE &&
										me.getY()+RAYON_ANTENNE >= position.getY() && me.getY() < position.getY() + RAYON_ANTENNE)
								{

									/** Si le IMSI Cliqué est une instance de IntercepteurOndes,
									 * il sera supprimer avec l'aide du compteur **/
									if (antenne instanceof IntercepteurOndes)
									{
										reseau.supprimerInterceptor(i);
										jeu.delete1ImsiPlace();
										nbrInterPlace--;
									}
								}
								i++;
							}
						}else{
							System.out.println("il n y a pas d'interceptor a supprimer");
						}
					}
					catch (Exception e)
					{
						//System.out.println("Erreur: " + e);
					}
				}
			}
		});
	}
		/**
         * Callback demandant de rafra�chir l'�cran
         */
	@Override
	public void avertir() {
		validate();
		repaint();
	}

}
