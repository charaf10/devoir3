package vue;

/**
 * Cadre principal qui contient deux panneaux, l'interface de contr�le en bande PAGE_START
 * et le panneau de dessin CENTER qui est int�gr� dans un JScrollPane
 * 
 * impl�mente Runnable
 * 
 * @author Fred Simard | ETS
 * @revision hiver 2021
 */


import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

@SuppressWarnings("serial")
public class CadrePrincipal extends JFrame implements Runnable{


	/** Instantiation de 2 panneaux, dessin et general **/
	JPanel panGeneral = new JPanel();
	JPanel panDessin2 = new JPanel();

	/** création d'une instance JScrollBar **/
	JTextArea txt = new JTextArea();
	JScrollPane scrollV = new JScrollPane();
	JScrollPane scrollH = new JScrollPane();



	/** instantiation du panneau Controle, qui contient les 3 panneaux de jeu **/
	PanControle panneaucontrole = new PanControle();

	/** instantiation du panneauInfoReseau **/
	PannInfoReseau panneauInfoReseau = new PannInfoReseau();

	/** instantiation du panneau OpérationIMSI **/
	OperationIMSI panoperationIMSI = new OperationIMSI();

	/** instantiation du panneau informationJeu **/
	InformationsJeu paninformationJeu = new InformationsJeu();

	/** Création d'une variable de type PanneauDessin **/
	PanneauDessin panneauDessin;

	//JPanel panneaucontrole = new JPanel();
	Dimension tailleEcran =	Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * T�che qui initialise le cadre
	 */
	@Override
	public void run() {

		initCadre();
		initPanneau();

    	setVisible(true);
	}
	
	/**
	 * M�thode qui initialise le cadre
	 */
	private void initCadre() {

    	// maximize la fenêtre
    	setExtendedState(JFrame.MAXIMIZED_BOTH);
		setSize(new Dimension(700,700));


		// ajoute une gestion du EXIT par confirmation pop-up
		this.addWindowListener(new WindowAdapter() {
		      
			// gestionnaire d'événement pour la fermeture
			public void windowClosing(WindowEvent we) {
				
				// ajoute une demande de confirmation
		        int result = JOptionPane.showConfirmDialog(null,
		            "Voulez-vous quitter?", "Confirmation de sortie: ",
		            JOptionPane.YES_NO_OPTION);
		        
		        // si la réponse est oui
		        if (result == JOptionPane.YES_OPTION){
		        	// ferme la fenêtre en activant la gestion de l'événement
		        	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        }
		        else if (result == JOptionPane.NO_OPTION){
		        	// sinon, désactive la gestion de l'événement
		        	setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		        }
		      }
		});
	}

	/**
	 * initialisation du contenu des deux panneaux PAGE_START et CENTER
	 */
	private void initPanneau() {

		setContentPane(panGeneral);
		panGeneral.setLayout(new BorderLayout());
		panGeneral.setBackground(Color.WHITE);

		panDessin2.setBackground(Color.gray);

		/** Instantiation du panneau dessin **/
		panneauDessin = new PanneauDessin(tailleEcran);
		panDessin2.add(panneauDessin);

		/** Ajout de JScrollBar au panneau dessin **/
		scrollV.setBackground(Color.BLUE);
		scrollV.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		panDessin2.add(scrollV);


		scrollH.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		panDessin2.add(scrollH);


		/** Ajout du panneau Controle au panneauGeneral qui contient tout **/
		panGeneral.add(panneaucontrole, BorderLayout.NORTH);

		/** Ajout des trois panneaux d'information au panneau de controle
		 * et positionnement selon les règles **/
		panneaucontrole.add(panneauInfoReseau, BorderLayout.WEST);
		panneaucontrole.add(paninformationJeu, BorderLayout.CENTER);
		panneaucontrole.add(panoperationIMSI, BorderLayout.EAST);

		/** Ajout du panneau dessin au panneauGeneral qui contient tout **/
		panGeneral.add(panDessin2, BorderLayout.CENTER);
	}

}
