package vue;

/**
 * InformationJeu est le panneau affichant les informations de la session de jeu
 * @author Achraf Zineddine Elidrissi | ETS
 */

import modele.gestionnaires.GestionnaireJeu;
import modele.gestionnaires.GestionnaireScenario;
import observer.MonObserver;
import tda.FileSChainee;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class InformationsJeu extends AbstractPanneauInfo implements MonObserver {


    /** Attribut et variable relatifs au jeu **/
    JProgressBar jProgress;
    JLabel Pointage, temps;
    int pointage, i = 0;

    ConstantesVue cvue = new ConstantesVue();
    GestionnaireScenario gestionaireScene = GestionnaireScenario.getInstance();


    /** Override la methode initPanneau de la classe abstraite parent **/
    @Override
    public void initPanneau(){
        this.setBackground(Color.gray);
        Border Whiteline = BorderFactory.createLineBorder(Color.white);
        this.setBorder(Whiteline);
        this.setLayout(new GridLayout(4,1));


        /**Ajout d'un JLabel Titre au panneau en appliquant le font de ConstantesVue **/
        Titre = new JLabel("Informations Jeu ", JLabel.CENTER);
        Titre.setFont(cvue.POLICE_TITRES);
        Titre.setForeground(Color.WHITE);

        /** Cr�ation et ajout d'un JLabel pour �coulement du temps **/
        temps = new JLabel("�coulement de temps", JLabel.CENTER);
        temps.setFont(cvue.POLICE_ETIQUETTES);
        temps.setForeground(Color.WHITE);

        /** Ajout d'un JProgessBar au panneau pour l'�coulement du temps **/
        jProgress = new JProgressBar();


        /** TIMER pour rafraichir le panneau informations jeu **/
        int retard = 250; // millisecondes
        ActionListener taskPerformer = new ActionListener () {
            public void actionPerformed (ActionEvent evt) {

                /** rappel la methode afficherPoint chaque 250ms **/
                afficherPoint();
            }
        };
        new Timer(retard,taskPerformer).start();
    }

@Override
    public void initContenu(){

        afficherPoint();
    }

    public void afficherPoint(){

        /** Instantiation d'une session de jeu de GestionnaireJeu **/
        GestionnaireJeu jeu = GestionnaireJeu.getInstance();


        /** Cr�ation et ajout d'un JLabel pour affichage de points **/
        Pointage = new JLabel("Pointage: " + pointage, JLabel.CENTER);
        Pointage.setFont(cvue.POLICE_ETIQUETTES);
        Pointage.setForeground(Color.WHITE);
        pointage = jeu.getnbPoint();


        /** Cr�ation et ajout d'un JprogressBar pour �coulement du temps **/
        jProgress = new JProgressBar();
        jProgress.setMaximum(267);
        jProgress.setValue(i);
        i+=1;
//        jProgress.setStringPainted(true);


        /** Mise � jour du panneau informationsJeu **/
        removeAll();
        updateUI();
        add(Titre);
        add(Pointage);
        add(temps);
        add(jProgress);
    }
}
