package vue;

/**
 * Constantes utilis�s dans la vue
 * 
 * @author Fred Simard | ETS
 * @revision hiver 2021
 */

import java.awt.*;

public class ConstantesVue {
	public static final Font POLICE_TITRES = new Font(null, Font.BOLD, 25);
	public static final Font POLICE_ETIQUETTES = new Font(null, Font.PLAIN, 18);
}
