package modele.reseau;

import modele.communication.Message;
import modele.gestionnaires.GestionnaireJeu;
import modele.gestionnaires.GestionnaireScenario;
import modele.physique.Position;

public class IntercepteurOndes extends Antenne{


    /** Création d'une instance de GestionnaireJeu **/
    GestionnaireJeu gestionjeu = GestionnaireJeu.getInstance();

    /**
     * constructeur d'une antenne
     *
     * @param position de l'antenne
     */
    public IntercepteurOndes(Position position) {
        super(position);
    }

    @Override
    public void envoyer(Message msg, int numeroConnexion)
    {

        // fait suivre le message à la carte
        reseau.relayerMessage(msg, numeroConnexion, this);


        /**Instantiation d'un objet GestionnaireScenario **/
        GestionnaireScenario gestionscenario = GestionnaireScenario.getInstance();

        /** Conversion du numero (int) en (String)  **/
        String num = String.valueOf(numeroConnexion);


        /** Methode pour verifier si un numéro est dans la liste des numeros de criminels **/
        /** Si oui, ajoute 1 point au pointage **/
        if (gestionscenario.numeroEstCriminel(num) != true)
        {
            gestionjeu.add1Point();
        }
    }
}


