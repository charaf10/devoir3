package modele.gestionnaires;

/**
 * Gestionnaire du jeu, impl�mente les fonctionnalit�s associ�s aux r�gles de la partie
 * jeux, tel que l'achat de IMSI et le pointage.
 * 
 * Impl�ment� comme un singleton
 * 
 * @author Fred Simard | ETS
 * @version H21
 */

import modele.reseau.GestionnaireReseau;
import observer.MonObservable;
import tda.FileSChainee;

public class GestionnaireJeu extends MonObservable{

	
	// �conomie du jeu
	public static int NB_IMSI_DEPART = 1;
	public static int COUT_IMSI = 10;

	// attributs associ�s au IMSI
	private int nbImsiRestant = 1;
	private int nbImsiPlace = 0;

	// attribut pointage
	private int nbPoints = 0;

	// r�f�rence au gestionnaire r�seau
	private GestionnaireReseau reseau;

	// instance (impl�mentation singleton)
	private static GestionnaireJeu instance = new GestionnaireJeu();

	// constructeur priv�e (impl�mentation singleton)
	private GestionnaireJeu(){
		reseau = GestionnaireReseau.getInstance();
	}

	// accesseur instance (impl�mentation singleton)
	public static GestionnaireJeu getInstance() {
		return instance;
	}

	/** methode pour recuperer le nombre de points de la session de jeu **/
	public int pointage()
	{
		return nbPoints;
	}


	/** methode qui ajoute 1 point lorsque IMSI detecte numero criminel **/
	public void add1Point(){  nbPoints++; }


	/** methode qui ajoute un IMSI lors d'un achat  **/
	public void add1ImsiRestant(){  nbImsiRestant++; }


	/** methode qui ajoute 1 IMCI lorsque ce dernier est plac� sur la carte **/
	public void add1ImsiPlace(){  nbImsiPlace++; }


	/** methode qui supprime 1 IMSI de la carte **/
	public void delete1ImsiPlace(){  nbImsiPlace--; }


	/** methode qui supprime le prix d'un IMSI du total de point lors d'un achat **/
	public void deletePointApresAchat(){  nbPoints-= COUT_IMSI; }


	/** methode pour recuperer le nombre de point de la session de jeu **/
	public int getnbPoint(){  return nbPoints; }


	/** methode pour recuperer le nombre de IMSI que le user possede dans cette game **/
	public int getnbImsiRestant(){  return nbImsiRestant; }


	/** methode pour recuperer le nombre de IMSI plac� sur la carte de la session de jeu **/
	public int getnbImsiplace(){  return nbImsiPlace; }

	/** methode pour recuperer le nombre de conversation durant la session de jeu **/
	public int getNbConversations(){ return reseau.getNbConversations(); }

}

























